from django.db import models
from django.conf import settings

# Create your models here.


class Project(models.Model):  # feature 3
    name = models.CharField(max_length=200)
    description = models.TextField(default="asdf")
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,  # feature 3
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.name}"
